#pragma once

#include "config_common.h"

#ifndef DEBOUNCING_DELAY
#    define DEBOUNCING_DELAY 10
#endif

#define FORCE_NKRO
//#define STRICT_LAYER_RELEASE
#define PERMISSIVE_HOLD
#define TAPPING_FORCE_HOLD
#define IGNORE_MOD_TAP_INTERRUPT
//#define RETRO_SHIFT 500

//#define AUTO_SHIFT_TIMEOUT 200

#ifdef TAPPING_TERM
#    undef TAPPING_TERM
#endif
#define TAPPING_TERM 200

#define MOUSEKEY_INTERVAL 16
#define MOUSEKEY_TIME_TO_MAX 100
#define MOUSEKEY_MAX_SPEED 5
#define MOUSEKEY_WHEEL_TIME_TO_MAX 100

#define EE_HANDS

#ifdef LOCKING_SUPPORT_ENABLE
#    undef LOCKING_SUPPORT_ENABLE
#endif
#ifdef LOCKING_RESYNC_ENABLE
#    undef LOCKING_RESYNC_ENABLE
#endif
#ifdef KEYBOARD_LOCK_ENABLE
#    undef KEYBOARD_LOCK_ENABLE
#endif

#ifndef NO_DEBUG
#    define NO_DEBUG
#endif
#ifndef NO_PRINT
#    define NO_PRINT
#endif
#ifndef NO_ACTION_MACRO
#    define NO_ACTION_MACRO
#endif
#ifndef NO_ACTION_FUNCTION
#    define NO_ACTION_FUNCTION
#endif
#ifndef NO_ACTION_TAPPING
//#    define NO_ACTION_TAPPING
#endif
#ifndef NO_ACTION_LAYER
//#    define NO_ACTION_LAYER
#endif
#ifndef NO_ACTION_ONESHOT
//#    define NO_ACTION_ONESHOT
#endif
